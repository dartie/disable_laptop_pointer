import subprocess
import re
import sys
import os

if os.sep == '/':  # Unix
	device_to_disable = 'Stick'  # value to search for identifying the device to disable
	regex_device_id = re.compile(r'id=(\d+)')
	my_env = os.environ.copy()
	my_env['DISPLAY'] = ':1'  # set env var needed by the xinput utility

	proc = subprocess.Popen(['xinput', "list"],
							shell=True,
							stdout=subprocess.PIPE,
							)
	stdout_value = proc.communicate()[0]
	stdout_value = stdout_value.decode("utf-8")  # converts the output to UTF-8
	stdout_value = stdout_value.split('\n')  # converts string to list

	device_id = None
	for l in stdout_value:
		if device_to_disable in l:
			print('Device {device} found : \n{line}\n\n'.format(device=device_to_disable, line=l))
			# get device id
			regex_id_matches = regex_device_id.findall(l)
			if len(regex_id_matches) != 0:
				device_id = regex_id_matches[0]
				break

	if device_id is None:
		print('Device "{device}" has not been found'.format(device=device_to_disable))
		sys.exit(2)
				
	command_disable_device = ["xinput", "disable", device_id]
	print(command_disable_device)
	os.system(" ".join(command_disable_device))


else:  # Windows
	print('Windows is not supported yet')
